"""
Download files from Google Drive
Code from StackOverflow https://stackoverflow.com/a/39225039/1366519
"""
import requests

def download(id, destination):
    def get_confirm_token(response):
        for key, value in response.cookies.items():
            if key.startswith('download_warning'):
                return value

        return None

    def save_response_content(response, destination):
        CHUNK_SIZE = 32768

        with open(destination, "wb") as f:
            for chunk in response.iter_content(CHUNK_SIZE):
                if chunk: # filter out keep-alive new chunks
                    f.write(chunk)

    URL = "https://docs.google.com/uc?export=download"

    session = requests.Session()

    response = session.get(URL, params = { 'id' : id }, stream = True)
    token = get_confirm_token(response)

    if token:
        params = { 'id' : id, 'confirm' : token }
        response = session.get(URL, params = params, stream = True)

    save_response_content(response, destination)    


if __name__ == "__main__":
    import sys
    if len(sys.argv) is not 3:
        print("Usage: python gdrive.py file_shareable_link /path/to/file.kml")
    else:
        # TAKE ID FROM SHAREABLE LINK
        file_id = sys.argv[1].split('id=')[1]
        # DESTINATION FILE ON YOUR DISK
        destination = sys.argv[2]
        download(file_id, destination)
